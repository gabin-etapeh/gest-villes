package com.gabi.gestiondevilles.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import java.util.Date;
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Ville {
    private int id;
    private String nom;
    private String description;
    private Date dateCreation;
    private Double population;
    private Double superficie;

}
