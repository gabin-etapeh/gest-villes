package com.gabi.gestiondevilles.service;

import com.gabi.gestiondevilles.entity.Ville;
import com.gabi.gestiondevilles.repository.VilleRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Service
public class VilleService {
    private final VilleRepository villeRepository;
    public VilleService (VilleRepository villeRepository){
        this.villeRepository = villeRepository;
    }
    public Ville addVille (Ville ville){
        return villeRepository.save(ville);
    }
    public Ville getVille (Integer id){
        return villeRepository.findById(id).orElse(null);
    }



}
