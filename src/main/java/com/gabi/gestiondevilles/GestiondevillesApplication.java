package com.gabi.gestiondevilles;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestiondevillesApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestiondevillesApplication.class, args);
	}

}
