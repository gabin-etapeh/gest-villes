package com.gabi.gestiondevilles.controler;

import com.gabi.gestiondevilles.entity.Ville;
import com.gabi.gestiondevilles.service.VilleService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class VilleController {
    private final VilleService villeService;
    public VilleController(VilleService villeService) {
        this.villeService = villeService;
    }

    @GetMapping("/ville/{id}")
    public ResponseEntity<Ville> getVille (@PathVariable Integer id){
        return new ResponseEntity<>(villeService.getVille(id), HttpStatus.OK);
    }
    @PostMapping("/ville")
    public ResponseEntity<Ville> addVille (@RequestBody Ville ville) {
        return new ResponseEntity<>(villeService.addVille(ville), HttpStatus.CREATED);
    }

}
